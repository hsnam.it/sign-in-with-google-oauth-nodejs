import React from 'react';
import GoogleLogin from 'react-google-login';
import {GOOGLE_CLIENT_ID, BACKEND_URL} from '../config/dev'
import axios from 'axios'

const GoogleLoginButton = () => {
    return (
            <GoogleLogin 
                clientId={GOOGLE_CLIENT_ID}
                buttonText="Login with Google"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
            />
    );
}

const responseGoogle = async (res) => {
    console.log('accessToken Google: ', res.accessToken);
    const accessToken = res.accessToken
    if (accessToken) {
        const result = await axios.post(`${BACKEND_URL}/auth/google`, {accessToken});
        return console.log("result: ", result);
    }
    console.log("result: Login failed");
}

export default GoogleLoginButton;
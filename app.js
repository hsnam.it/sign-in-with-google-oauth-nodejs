const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const google = require('googleapis').google;


const app = new express();
app.use(bodyParser.json());
app.use(cors());

app.get('/', (req, res) => {
    res.send('You know, for OAuth testing! \n');
});

app.post('/api/auth/google', async (req, res) => {
    let oauth2Client = new google.auth.OAuth2();
    oauth2Client.setCredentials({access_token: req.body.accessToken})

    let oauth2 = google.oauth2({
        auth: oauth2Client,
        version: 'v2'
    });

    const userInfo = await oauth2.userinfo.get();
    console.log('userInfo', userInfo.data);

    res.send({status: 'logged in'});
});

app.listen(4000, () => {
    console.log('Server started on port 4000');
});